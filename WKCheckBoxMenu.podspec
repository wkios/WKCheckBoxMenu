

Pod::Spec.new do |s|
  s.name         = "WKCheckBoxMenu"
  s.version      = "0.0.1"
  s.summary      = "A short description of WKCheckBoxMenu."
  s.description  = <<-DESC
                简单的下拉选择列表
                   DESC

  s.homepage     = "https://gitee.com/wkios/WKCheckBoxMenu.git"

  s.license      = "MIT"

  s.author             = { "fanwenkai" => "ios_fanwenkai@163.com" }

  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/wkios/WKCheckBoxMenu.git", :tag => "#{s.version}" }

  s.source_files  = "WKCheckBoxMenu/*.swift"

  s.frameworks = 'UIKit', 'QuartzCore', 'Foundation'
  s.module_name = 'WKCheckBoxMenu'

  s.requires_arc = true

  # s.dependency "JSONKit", "~> 1.4"    #依赖关系，该项目所依赖的其他库，如果有多个可以写多个 s.dependency

end
